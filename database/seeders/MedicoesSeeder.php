<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MedicoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('medicoes')->insert(
            [
                [
                    'pilha_bateria' => 'Pilha Alcalina Duracel AA',
                    'tensao_nominal' => 1.5,
                    'capacidade_corrente' => 2800, 
                    'tensao_sem_carga' => 1.30,
                    'tensao_com_carga' => 1.29,
                    'resistencia_carga' => 24.1,
                ],
                [
                    'pilha_bateria' => 'Pilha Jyx',
                    'tensao_nominal' => 4.2,
                    'capacidade_corrente' => 9800,
                    'tensao_sem_carga' => 2.5,
                    'tensao_com_carga' => 2.473,
                    'resistencia_carga' => 24.1,
                ],
                [
                    'pilha_bateria' => 'Pilha Luatek',
                    'tensao_nominal' => 3.7,
                    'capacidade_corrente' => 1200,
                    'tensao_sem_carga' => 2.52,
                    'tensao_com_carga' => 2.48,
                    'resistencia_carga' => 24.1,
                ],
                [
                    'pilha_bateria' => 'Pilha Phillips AAA',
                    'tensao_nominal' => 1.5,
                    'capacidade_corrente' => 1200,
                    'tensao_sem_carga' => 1.37,
                    'tensao_com_carga' => 1.31,
                    'resistencia_carga' => 24.1,
                ],
                [
                    'pilha_bateria' => 'Pilha Panasonic',
                    'tensao_nominal' => 1.5,
                    'capacidade_corrente' => 1200,
                    'tensao_sem_carga' => 1.38,
                    'tensao_com_carga' => 1.34,
                    'resistencia_carga' => 24.1,
                ],
                [
                    'pilha_bateria' => 'Bateria Golite',
                    'tensao_nominal' => 9,
                    'capacidade_corrente' => 500,
                    'tensao_sem_carga' => 5.88,
                    'tensao_com_carga' => 2.87,
                    'resistencia_carga' => 24.1,
                ],
                [
                    'pilha_bateria' => 'Bateria Elgin 9v',
                    'tensao_nominal' => 9,
                    'capacidade_corrente' => 250,
                    'tensao_sem_carga' => 7.2,
                    'tensao_com_carga' => 2.13,
                    'resistencia_carga' => 24.1,
                ],
                [
                    'pilha_bateria' => 'Bateria unipower',
                    'tensao_nominal' => 12,
                    'capacidade_corrente' => 7000,
                    'tensao_sem_carga' => 10.49,
                    'tensao_com_carga' => 10.32,
                    'resistencia_carga' => 24.1,
                ],
                [
                    'pilha_bateria' => 'Bateria Freedom',
                    'tensao_nominal' => 12,
                    'capacidade_corrente' => 300,
                    'tensao_sem_carga' => 10.68,
                    'tensao_com_carga' => 10.65,
                    'resistencia_carga' => 24.1,
                ],
                
            ]
        );
    }
}

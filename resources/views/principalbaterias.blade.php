@extends('templates.base')

@section('conteudo')
<body>
    <main>
        <h1>Turma: 2D2 - Grupo 3</h1>
        <h2>Participantes:</h2>
        
        <hr>

        <table class="table table-bordered table-striped">
            <tr class="table-dark">
                <td>MAtrícula</td>
                <td>Nome</td>
                <td>Função</td>
            </tr>
            <tr>
                <td>0072546</td>
                <td>Pedro Lucas Silva de Oliveira</td>
                <td>HTML</td>
            </tr>
            <tr>
                <td>0073023</td>
                <td>Saymon Rodrigues da Silva</td>
                <td>Banco de dados</td>
            </tr>  
            <tr>
                <td>0073581</td>
                <td>Thúlio Vinícius Barboza</td>
                <td>Gerente</td>
            </tr>   
            <tr>
                <td>0073009</td>
                <td>Yann Ferreira Guimarães Rocha</td>
                <td>CSS</td>
            </tr>
        </table>

        <img src="{{ asset('imgs/trabalho2d2.jpeg')}}" width="400px" alt="Componentes do Grupo">
    </main>

@endsection

@section('footlose')
<h4> Rodapé da Página principal </h4>
@endsection